// swift-tools-version:5.3
import PackageDescription

let package = Package(
  name: "MKLogger",
  platforms: [
    .iOS(.v13),
    .macOS(.v10_15)
  ],
  products: [
    .library(
      name: "MKLogger",
      targets: ["MKLogger"])
  ],
  targets: [
    .target(
      name: "MKLogger",
      path: "Sources",
      exclude: ["Info.plist"]),
    .testTarget(
      name: "MKLoggerTests",
      dependencies: ["MKLogger"],
      path: "MKLoggerTests",
      exclude: ["Info.plist"])
  ]
)

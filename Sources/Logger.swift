//
//  Logger.swift
//  MKLogger
//
//  Created by Ленар Гилязов on 23.11.2020.
//

import Foundation
import os

// MARK: - LogEvent

public enum LogEvent: String {
  case error = "[‼️]"
  case info = "[ℹ️]"
  case debug = "[💬]"
  case verbose = "[🔬]"
  case warning = "[⚠️]"
  case severe = "[🔥]"
}

// MARK: - Logger

final public class Logger {

  // MARK: Public

  public class func log(
    message: String,
    event: LogEvent,
    fileName: String = #file,
    line: Int = #line,
    column: Int = #column,
    funcName: String = #function)
  {
    let log = OSLog(subsystem: "com.movika.logger.ios", category: "main")
    os_log("%@[%@]:%d %d %@ -> %@", log: log, type: .debug, event.rawValue, sourceFileName(filePath: fileName), line, column, funcName, message)
  }

  // MARK: Private

  private class func sourceFileName(filePath: String) -> String {
    let components = filePath.components(separatedBy: "/")
    guard let fileName = components.last, !components.isEmpty else { return "" }
    return fileName
  }
  
}

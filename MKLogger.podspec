Pod::Spec.new do |spec|

  spec.name         = "MKLogger"
  spec.version      = "1.0.2"
  spec.summary      = "Movika logger"

  spec.description  = "A short description of logger."
  spec.swift_version = '5.3'

  spec.homepage     = "https://movika.com/"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author             = { "Lenar Gilyazov" => "lenar.gilyazov@movika.com" }
  spec.source = { :git => 'https://bitbucket.org/lenar-gilyazov/mklogger.git', :tag => spec.version.to_s }

  spec.ios.deployment_target = "13.0"
  spec.osx.deployment_target  = "10.15"
  
  spec.source_files  =  'Sources/**/*.{swift}'
  
  spec.test_spec 'Tests' do |test_spec|
    test_spec.source_files = 'MKLoggerTests/**/*.{swift}'
  end

end


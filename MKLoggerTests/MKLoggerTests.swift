//
//  MKLoggerTests.swift
//  MKLoggerTests
//
//  Created by Ленар Гилязов on 23.11.2020.
//

import XCTest
@testable import MKLogger

class MKLoggerTests: XCTestCase {

    func testLog() throws {
      Logger.log(message: "Test message", event: .info)
    }

}
